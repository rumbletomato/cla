package main

import (
	"bitbucket.org/rumbletomato/cla.git/internal/app/cla"
	"flag"
	"fmt"
	"github.com/BurntSushi/toml"
	"log"
)

var (
	configPath       string
	confluenceLogin  string
	confluenceToken  string
	confluenceDomain string
	dbUsername       string
	dbPassword       string
	dbHost           string
	dbName           string
)

func init() {
	flag.StringVar(&configPath, "config-path", "configs/cla.toml", "path to config file")
	flag.StringVar(&confluenceLogin, "confluence-login", "", "confluence user login")
	flag.StringVar(&confluenceToken, "confluence-token", "", "confluence app token")
	flag.StringVar(&confluenceDomain, "confluence-domain", "", "confluence domain name")
	flag.StringVar(&dbUsername, "db-username", "", "database user name")
	flag.StringVar(&dbPassword, "db-password", "", "database password")
	flag.StringVar(&dbHost, "db-host", "", "database host")
	flag.StringVar(&dbName, "db-name", "", "database name")
}

func parseArgs(config *cla.Config) error {
	flag.Parse()

	_, err := toml.DecodeFile(configPath, config)
	if err != nil {
		log.Fatal(err)
	}

	if confluenceLogin != "" {
		config.Confluence.Login = confluenceLogin
	}

	if confluenceToken != "" {
		config.Confluence.AppToken = confluenceToken
	}

	if confluenceDomain != "" {
		config.Confluence.AppToken = confluenceDomain
	}

	if config.Confluence.Login == "" {
		return fmt.Errorf("ConfluenceLogin must not be empty")
	}

	if config.Confluence.AppToken == "" {
		return fmt.Errorf("ConfluenceAppToken must not be empty")
	}

	if config.Confluence.DomainName == "" {
		return fmt.Errorf("ConfluenceDomainName must not be empty")
	}

	if config.Database.Username == "" {
		return fmt.Errorf("DatabaseUsername must not be empty")
	}

	if config.Database.Password == "" {
		return fmt.Errorf("DatabasePassword must not be empty")
	}

	if config.Database.Host == "" {
		return fmt.Errorf("DatabaseHost must not be empty")
	}

	if config.Database.Name == "" {
		return fmt.Errorf("DatabaseName must not be empty")
	}

	return nil
}

func main() {
	config := cla.NewConfig()
	if err := parseArgs(config); err != nil {
		log.Fatal(err)
	}

	claProject := cla.NewCLAProject(config)
	if err := claProject.Run(); err != nil {
		log.Fatal(err)
	}
}
