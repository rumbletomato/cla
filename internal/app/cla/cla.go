package cla

import (
	"bitbucket.org/rumbletomato/cla.git/internal/app/cla/models"
	"bitbucket.org/rumbletomato/cla_car"
	confluencego "github.com/VitMovie/Confluence-Collector"
	"github.com/gin-contrib/multitemplate"
	"github.com/gin-gonic/gin"
	articlesgo "github.com/vitmovie/articlesgo"
	"gitlab.com/Faincer/wikipediation"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

type CLAProject struct {
	config *Config
	db     *articlesgo.Database
}

func NewCLAProject(config *Config) *CLAProject {
	models.DB = articlesgo.NewDatabase(
		config.Database.Username,
		config.Database.Password,
		config.Database.Host,
		config.Database.Name)

	models.DB.Connection.AutoMigrate(&articlesgo.Article{})

	cla := &CLAProject{
		config: config,
		db:     models.DB,
	}

	return cla
}
func createMyRender() multitemplate.Renderer {
	r := multitemplate.NewRenderer()
	r.AddFromFiles("index", "templates/base.html", "templates/navbar.html", "templates/index.html")
	r.AddFromFiles("article_diff", "templates/base.html", "templates/navbar.html", "templates/article_diff.html")

	return r
}

func containsInt(s []int, elem int) bool {
	for _, a := range s {
		if a == elem {
			return true
		}
	}

	return false
}

func (cla *CLAProject) Run() error {
	gin.SetMode(gin.ReleaseMode)

	router := gin.Default()
	router.HTMLRender = createMyRender()
	router.GET("/", func(context *gin.Context) {
		article := articlesgo.Article{DB: models.DB}
		allArticlesMeta, err := article.GetAllArticles()
		if err != nil {
			log.Fatal(err)
		}

		context.HTML(http.StatusOK, "index", gin.H{
			"title":           "Confluence Load Articles",
			"brandName":       "CLA",
			"allArticlesMeta": allArticlesMeta,
		})
	})

	router.GET("/articles/:articleID", func(context *gin.Context) {
		CLACar := cla_car.NewCLACar(
			cla.config.Confluence.Login,
			cla.config.Confluence.AppToken,
			cla.config.Confluence.DomainName)

		articleIDStr := context.Param("articleID")
		articleID, err := strconv.ParseUint(articleIDStr, 10, 32)
		if err != nil {
			log.Fatal(err)
		}

		article := new(articlesgo.Article)
		cla.db.Connection.Where(&articlesgo.Article{ConfluenceID: int(articleID)}).First(&article)
		articleTitle := article.Title

		articleOriginal, err := CLACar.GetArticleBody(int32(articleID))
		if err != nil {
			log.Fatal(err)
		}

		articleModified := wikipediation.Wikipediation(cla.db, articleOriginal)
		context.HTML(http.StatusOK, "article_diff", gin.H{
			"title":           "Confluence Load Articles",
			"brandName":       "CLA",
			"articleOriginal": template.HTML(articleOriginal),
			"articleModified": template.HTML(articleModified),
			"articleTitle":    articleTitle,
		})
	})

	router.GET("/do_index", func(context *gin.Context) {
		var articles []articlesgo.Article
		articles = confluencego.CollectArticles(
			cla.config.Confluence.Login,
			cla.config.Confluence.AppToken,
			cla.config.Confluence.DomainName)

		metaArticle := articlesgo.Article{DB: models.DB}
		knownArticles, _ := metaArticle.GetAllArticles()
		var knownArticleIDs []int
		for _, article := range knownArticles {
			knownArticleIDs = append(knownArticleIDs, article.ConfluenceID)
		}

		for _, article := range articles {
			if containsInt(knownArticleIDs, article.ConfluenceID) {
				continue
			}

			article.DB = cla.db
			if _, err := article.CreateArticle(); err != nil {
				log.Fatal(err)
			}
		}

		context.Redirect(http.StatusFound, "/")
	})

	if err := router.Run(cla.config.BindAddr); err != nil {
		log.Fatal(err)
	}

	return nil
}
