package cla

// Config ...
type Config struct {
	BindAddr   string     `toml:"bind_addr"`
	LogLevel   string     `toml:"log_level"`
	Confluence confluence `toml:"confluence"`
	Database   database   `toml:"database"`
}

type confluence struct {
	Login      string `toml:"login"`
	AppToken   string `toml:"app_token"`
	DomainName string `toml:"domain_name"`
}

type database struct {
	Username string `toml:"username"`
	Password string `toml:"password"`
	Host     string `toml:"host"`
	Name     string `toml:"dbname"`
}

// NewConfig ...
func NewConfig() *Config {
	return &Config{
		BindAddr: ":8080",
		LogLevel: "debug",
		Confluence: confluence{
			Login:      "",
			AppToken:   "",
			DomainName: "",
		},
		Database: database{
			Username: "",
			Password: "",
			Host:     "",
			Name:     "",
		},
	}
}
