# Confluence Article Linkerbot

## Install
1. In command line execute command `make` or `make build` from root directory
2. Create mysql database. For example, named `confluence`
3. Fill `configs/cla.toml` with credentials to database and confluence linkerbot accounts

## Launch
1. In command line execute command `cla.exe` or corresponding for your OS

## Use
1. Go to `localhost:8080` (or other port, depends on configs)
2. Click button `Do Index`
![Empty](./docs/cla_0.PNG)
3. Click on one of the selected article in list
![Indexed](./docs/cla_1.PNG)
4. To return on main screen click to logo `CLA`
![ArticleDiff](./docs/cla_2.PNG)
